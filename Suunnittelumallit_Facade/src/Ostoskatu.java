
public class Ostoskatu implements Nähtävyydet {
	
	private int hinta = 0;

	@Override
	public String esitteleNähtävyys() {
		return ("Leveä kävelykatu muotiliikkeineen sekä käsityöpajoineen. ");
	}

	@Override
	public String kerroSijainti() {
		return ("Ostoskatu sijaitsee vanhankaupungin keskustassa. Alkaa tuomiokirkolta ja päättyy satamaan.");
	}

	@Override
	public int laskeHinta(int lkm) {
		return hinta*lkm;
	}

}
