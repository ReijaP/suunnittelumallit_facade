
public class LinnanRauniot implements Nähtävyydet {
	
	private int hinta = 13;

	@Override
	public String esitteleNähtävyys() {
		return ("Keskiaikaisen linnan rauniot. ");
	}

	@Override
	public String kerroSijainti() {
		return ("Linnan rauniot sijaisevat vanhankaupungin itälaidalla, tuomiokirkkoa vastapäätä");
	}

	@Override
	public int laskeHinta(int lkm) {
		return hinta*lkm;
	}

}
