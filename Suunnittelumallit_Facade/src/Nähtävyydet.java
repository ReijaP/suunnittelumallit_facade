
public interface Nähtävyydet {
	
	public String esitteleNähtävyys();
	public String kerroSijainti();
	public int laskeHinta(int lkm);

}
