
public class Tuomiokirkko implements Nähtävyydet {
	
	private int hinta = 2;

	@Override
	public String esitteleNähtävyys() {
		return ("1700-luvulla rakennettu tuomiokirkko kellotorneineen. ");
	}
	
	public String kerroSijainti() {
		return ("Tuomiokirkko sijaitsee kaupungin itäportin kupeessa, keskustorin laidalla.");
	}

	@Override
	public int laskeHinta(int lkm) {
		return hinta*lkm;
	}

}
