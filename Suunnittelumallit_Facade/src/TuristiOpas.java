//Facade
public class TuristiOpas {
	
	private Nähtävyydet linnanrauniot = new LinnanRauniot();
	private Nähtävyydet ostoskatu = new Ostoskatu();
	private Nähtävyydet kirkko = new Tuomiokirkko();
	private int hinta = 0;
	
	public void esitteleKaupunki(int lkm) {
		System.out.println("Kaupungissa voi tutustua seuraaviin nähtävyyksiin:\n");
		
		System.out.println(linnanrauniot.esitteleNähtävyys() + linnanrauniot.kerroSijainti());
		System.out.println("Sisäänpääsymaksu " + lkm + " henkilöltä on " + linnanrauniot.laskeHinta(lkm) + "€");
		
		System.out.println(ostoskatu.esitteleNähtävyys() + ostoskatu.kerroSijainti());
		System.out.println("Sisäänpääsymaksu " + lkm + " henkilöltä on " + ostoskatu.laskeHinta(lkm) + "€");
		
		System.out.println(kirkko.esitteleNähtävyys() + kirkko.kerroSijainti());
		System.out.println("Sisäänpääsymaksu " + lkm + " henkilöltä on " + kirkko.laskeHinta(lkm) + "€");
	}

}
